build:
	@echo "\n\e[1m\e[32m-- Building docker images --\e[22m\e[24m\e[39m"
	docker-compose build
	
setup:
	@echo "\n\e[1m\e[32m-- Mounting containers --\e[22m\e[24m\e[39m"
	docker-compose up -d

clean:
	@echo "\n\e[1m\e[32m-- Removing containers --\e[22m\e[24m\e[39m"
	docker-compose down --remove-orphans

remove:
	make clean
	@echo "\n\e[1m\e[32m-- Pruning docker images and networks --\e[22m\e[24m\e[39m"
	docker system prune -a

configure:
########### IPv4 config ########### 	
	@echo "\n\e[1m\e[32m-- Configuring default IPv4 gateway on nodes --\e[22m\e[24m\e[39m"
# r1
	docker exec -ti router_R1 ip route del default;
# pc1.edu
	docker exec -ti host_H1 ip route del default;
	docker exec -ti host_H1 ip route add default via 192.168.2.10;
# ns1.edu
	docker exec -ti dns1_primario ip route del default;
	docker exec -ti dns1_primario ip route add default via 192.168.1.10;

########### IPv6 config ########### 
	@echo "\n\e[1m\e[32m-- Configuring default IPv6 gateway on nodes --\e[22m\e[24m\e[39m"
# pc1.edu
	docker exec -ti host_H1 ip -6 route del default;
	docker exec -ti host_H1 ip -6 route add default via 2001:b:b:2::10;
# ns1.edu
	docker exec -ti dns1_primario ip -6 route del default;
	docker exec -ti dns1_primario ip -6 route add default via 2001:a:a:1::10;

check:
	@echo "\n\e[1m\e[32m-- Checking IPv4 and IPv6 connectivity --\e[22m\e[24m\e[39m"

# tests desde pc1.edu a ns1.edu
	@echo "\n\e[1m\e[32mping (IPv4) de pc1 a ns1\e[22m\e[24m\e[39m"
	docker exec -ti host_H1 ping -c 3 192.168.1.2;
	@echo "\n\e[1m\e[32mping (IPv6) de pc1 a ns1\e[22m\e[24m\e[39m"
	docker exec -ti host_H1 ping6 -c 3 2001:a:a:1::2;
	@echo "\n\e[1m\e[32mnslookup de pc1 a ns1\e[22m\e[24m\e[39m"
	docker exec -ti host_H1 nslookup ns1.edu
	@echo "\e[1m\e[32mnslookup inverso (IPv4) de pc1 a ns1\e[22m\e[24m\e[39m"
	docker exec -ti host_H1 nslookup 192.168.1.2
	@echo "\e[1m\e[32mnslookup inverso (IPv6) de pc1 a ns1\e[22m\e[24m\e[39m"
	docker exec -ti host_H1 nslookup 2001:a:a:1::2

# tests desde ns1.edu a pc1.edu
	@echo "\e[1m\e[32mping (IPv4) de ns1 a pc1\e[22m\e[24m\e[39m"
	docker exec -ti dns1_primario ping -c 3 192.168.2.2;
	@echo "\n\e[1m\e[32mping (IPv6) de ns1 a pc1\e[22m\e[24m\e[39m"
	docker exec -ti dns1_primario ping6 -c 3 2001:b:b:2::2;
	@echo "\n\e[1m\e[32mnslookup de ns1 a pc1\e[22m\e[24m\e[39m"
	docker exec -ti dns1_primario nslookup pc1.edu
	@echo "\e[1m\e[32mnslookup inverso (IPv4) de ns1 a pc1\e[22m\e[24m\e[39m"
	docker exec -ti dns1_primario nslookup 192.168.2.2
	@echo "\e[1m\e[32mnslookup inverso (IPv6) de ns1 a pc1\e[22m\e[24m\e[39m"
	docker exec -ti dns1_primario nslookup 2001:b:b:2::2

# tests desde pc1.edu a tp6redes.edu
	@echo "\e[1m\e[32mping (IPv4) de pc1 a tp6redes\e[22m\e[24m\e[39m"
	docker exec -ti host_H1 ping -c 3 192.168.3.2;
	@echo "\n\e[1m\e[32mping (IPv6) de pc1 a tp6redes\e[22m\e[24m\e[39m"
	docker exec -ti host_H1 ping6 -c 3 2001:c:c:3::2;
	@echo "\n\e[1m\e[32mnslookup de pc1 a tp6redes\e[22m\e[24m\e[39m"
	docker exec -ti host_H1 nslookup tp6redes.edu
	@echo "\e[1m\e[32mnslookup inverso (IPv4) de pc1 a tp6redes\e[22m\e[24m\e[39m"
	docker exec -ti host_H1 nslookup 192.168.3.2
	@echo "\e[1m\e[32mnslookup inverso (IPv6) de pc1 a tp6redes\e[22m\e[24m\e[39m"
	docker exec -ti host_H1 nslookup 2001:c:c:3::2

	@echo "\e[1m\e[32m-- Checking strapi --\e[22m\e[24m\e[39m"

# tests para verificar que funcione bien strapi	
	@echo "\n\e[1m\e[32mcurl a http://tp6redes.edu\e[22m\e[24m\e[39m"
	docker exec -ti host_H1 curl http://tp6redes.edu;
	@echo "\n\e[1m\e[32mcurl a https://tp6redes.edu\e[22m\e[24m\e[39m"
	docker exec -ti host_H1 curl --cacert /etc/ssl/certs/tp6redes-edu.pem https://tp6redes.edu;
	
	@echo "\n\e[1m\e[32mPOST para autenticar user usando http\e[22m\e[24m\e[39m"
	docker exec -ti host_H1 curl -H "Content-Type: application/json" -X POST -d '{"identifier": "test@gmail.com","password":"testtest"}' http://tp6redes.edu/auth/local
	@echo "\n\n\e[1m\e[32mPOST para autenticar user usando https\e[22m\e[24m\e[39m"
	docker exec -ti host_H1 curl --cacert /etc/ssl/certs/tp6redes-edu.pem -H "Content-Type: application/json" -X POST -d '{"identifier": "test@gmail.com","password":"testtest"}' https://tp6redes.edu/auth/local
	
	@echo "\n\n\e[1m\e[32mcurl a https://tp6redes.edu/alumnos/5ee14c9d9203290098c59cd2 sin autenticar\e[22m\e[24m\e[39m"
	docker exec -ti host_H1 curl --cacert /etc/ssl/certs/tp6redes-edu.pem -X GET https://tp6redes.edu/alumnos/5ee14c9d9203290098c59cd2;
	@echo "\n\n\e[1m\e[32mcurl a https://tp6redes.edu/alumnos/5ee14c9d9203290098c59cd2 autenticado\e[22m\e[24m\e[39m"
	docker exec -ti host_H1 curl --cacert /etc/ssl/certs/tp6redes-edu.pem -X GET https://tp6redes.edu/alumnos/5ee14c9d9203290098c59cd2 -H "Authorization: Bearer eeyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVlZTExMjYxYmZmNDJiMDA4ODNmMGIyZiIsImlhdCI6MTU5MTg3OTYyOSwiZXhwIjoxNTk0NDcxNjI5fQ.ufBprY7tAkfK0qUM8Ycom3Idl5rqSp-Jj0ygtBOPJw4";
	@echo "\n"

reconfigure:
	make clean
	make build
	make setup
	make configure
	make check